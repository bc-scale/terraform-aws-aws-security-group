output "security_group_id_ssh" {
  description = "This is the SSH AWS Security Group ID"
  value       = aws_security_group.ssh.id
}

output "security_group_id_web" {
  description = "This is the WEB AWS Security Group ID"
  value       = aws_security_group.web.id
}
